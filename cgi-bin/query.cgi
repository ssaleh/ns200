#!/bin/bash -f

# host name
host="`echo $QUERY_STRING | awk -F= '{print $2}'`"

# if hostname does not query 
invalid_host=`dig $host +noall +answer | grep -v "^;"`

echo "Content-type: text/html"

echo ""

echo "<html>"

echo "<head>"
echo "<style>"
echo "a.ex1:hover, a.ex1:active {color: red;}"
echo "</style>"
echo "</head>"

echo "<body>"

echo "<h1><p align=\"center\"><img src=\"http://ns200.com/ns200.jpg\" alt=\"Oops, dropped my pants\"> </h1>"

echo "<table align="center" border ="1" bordercolor="#034f84" width="90%">"
    echo "<col width="100">"
    echo "<col width="100">"
    echo "<col width="100">"
  echo "<tr>"
      echo "<td align="center"> <a class="ex1" href="http://ns200.com/query.html">Query</a></td>"
      echo "<td align="center"> <a class="ex1" href="http://ns200.com">Home</a></td>"
      echo "<td align="center"> <a class="ex1" href="http://ns200.com/rfc.html">RFC</a></td>"
  echo "</tr>"
echo "</table>"

# echo "<br><hr WIDTH=\"90%\" SIZE=\"3\">"

if [[ "$host" = "" || -z $invalid_host ]] ; then

# The following is only processed if the input string is an invalid domain!

  echo "<p align="center"><img src="http://ns200.com/sadface.jpg" alt="You dumbass!!"> </p>"
  echo "<p align=\"center\">Invalid Option. Try again.</p>"
  echo "</body></html>"

else

#   host="`echo $QUERY_STRING | awk -F= '{print $2}'`"

  # Parent Domain
  pd=`echo $host | cut -f2,3,4,5,6 -d.`

  #Parent Domain Name Server
  pdns="`dig ns $pd +noall +answer | tail -1 | awk '{print $NF}'`"

  #Domain name servers
  dns="dig $host ns +noall +answer"

  echo "<br><br>"

  # Table Starts here
  echo "<table border ="1" width="90%" bordercolor="#034f84" align="center">"

  # Header Row
  echo "<tr> <td> Category </td> <td valign="top"> Status </td> <td> Test Name </td> <td> Information </td> </tr>"

  # Parent Domain Details - A1
  A=`dig ns $host @$pdns +noall +auth| grep -v ";" | sort | while read i; do echo "$i <br>"; done  | sed '1d'`;  
  echo "<tr> <td valign="top" rowspan="1"> Root </td> <td valign="top"> Status </td> <td valign="top"> Root domains</td> <td> 

<table>

<tr><td>Host               </td><td>	IPv4 Address </td><td>  IPv6 Address  </td><td> 	Hosting Organization </td></tr>
<tr><td>a.root-servers.net </td><td>	198.41.0.4 </td><td>  2001:503:ba3e::2:30 </td><td> 	VeriSign, Inc. </td></tr>
<tr><td>b.root-servers.net </td><td> 	199.9.14.201 </td><td>  2001:500:200::b </td><td> 	University of Southern California (ISI)</td></tr>
<tr><td>c.root-servers.net </td><td> 	192.33.4.12 </td><td>  2001:500:2::c </td><td> 	Cogent Communications</td></tr>
<tr><td>d.root-servers.net </td><td> 	199.7.91.13 </td><td>  2001:500:2d::d </td><td> 	University of Maryland</td></tr>
<tr><td>e.root-servers.net </td><td> 	192.203.230.10 </td><td>  2001:500:a8::e </td><td> 	NASA (Ames Research Center)</td></tr>
<tr><td>f.root-servers.net </td><td> 	192.5.5.241 </td><td>  2001:500:2f::f </td><td> 	Internet Systems Consortium, Inc.</td></tr>
<tr><td>g.root-servers.net </td><td> 	192.112.36.4 </td><td>  2001:500:12::d0d </td><td> 	US Department of Defense (NIC)</td></tr>
<tr><td>h.root-servers.net </td><td> 	198.97.190.53 </td><td>  2001:500:1::53 </td><td> 	US Army (Research Lab)</td></tr>
<tr><td>i.root-servers.net </td><td> 	192.36.148.17 </td><td>  2001:7fe::53 </td><td> 	Netnod</td></tr>
<tr><td>j.root-servers.net </td><td> 	192.58.128.30 </td><td>  2001:503:c27::2:30 </td><td> 	VeriSign, Inc.</td></tr>
<tr><td>k.root-servers.net </td><td> 	193.0.14.129 </td><td>  2001:7fd::1 </td><td> 	RIPE NCC</td></tr>
<tr><td>l.root-servers.net </td><td> 	199.7.83.42 </td><td>  2001:500:9f::42 </td><td> 	ICANN</td></tr>
<tr><td>m.root-servers.net </td><td> 	202.12.27.33 </td><td>  2001:dc3::35 </td><td> 	WIDE Project</td></tr>

</table>

</td></tr>" ;
  echo "<tr> <td valign="top" rowspan="2"> Parent </td> <td valign="top"> Status </td> <td valign="top"> NS Records from the parent domain</td> <td>" ;
  #dig ns $host @$pdns +noall +add | grep -v ";" | sort | while read i; do echo "$i <br>"; done  | sed '1d';  
  echo $A
  echo "<br><br>Details provided by $pdns</td> </tr>"
  
  # Parent Domain Details - A2
  if [ -z $A ]
   then 
     A2="Bad, parent domain does not have your A records."
   else
     A2="Good, parent domain has your A records."
  fi

  echo "<tr> <td valign="top"> Status </td> <td valign="top"> NS Records from the parent domain</td> <td>";
  printf "$A2 </td> </tr>"
  # printf "<tr> <td valign="top"> Status </td> <td> NS Records from the parent domain</td> <td> ",$A2,"</td> </tr>";

  # domain name servers are here - B1
  echo "<tr> <td valign="top" rowspan="2"> NS </td> <td valign="top"> Status </td> <td valign="top"> NS records from the domain nameservers </td> <td>" ;  dig ns $host +noall +answer | grep -v ";" |  awk '{print $NF, $0}' | sort -n | while read i; do echo "$i <br>"; done  | sed '1d'; echo "</td> </tr>"

  # SOA Details
  soa_server=`dig $host soa +noall +answer | tail -1 | awk '{print $5}'`;
  soa_hostmaster=`dig $host soa +noall +answer | tail -1 | awk '{print $6}'`;
  soa_serial=`dig $host soa +noall +answer | tail -1 | awk '{print $6}'`;
  soa_refresh=`dig $host soa +noall +answer | tail -1 | awk '{print $7}'`;
  soa_retry=`dig $host soa +noall +answer | tail -1 | awk '{print $8}'`;
  soa_expire=`dig $host soa +noall +answer | tail -1 | awk '{print $9}'`;
  soa_default=`dig $host soa +noall +answer | tail -1 | awk '{print $10}'`;
  

  # RQuery=`dig verisign.com @

  echo "<tr>  <td valign="top"> Status </td> <td> Recursive Queries</td> <td>The SOA record is:<br>"; 

  echo "<tr> <td valign=top> SOA </td> <td valign="top"> Status </td> <td valign="top"> Start of Authority Records</td> <td>The SOA record is:<br>"; 
  echo "Primary nameserver: "
  echo $soa_server; 
  echo "<br>Hostmaster E-mail address: ";
  echo $soa_hostmaster;
  echo "<br>Serial: ";
  echo $soa_serial;
  echo "<br>Refresh:  ";
  echo $soa_refresh;
  echo "<br>Retry: ";
  echo $soa_retry;
  echo "<br>Expire: ";
  echo $soa_expire;
  echo "<br>Default TTL: ";
  echo $soa_default;
  echo " </td> </tr>"
  
  # MX Details
  echo "<tr> <td valign="top" rowspan="5"> MX </td> <td valign="top"> Status </td> <td valign="top"> Mail Server Records </td> <td>" 
  mx_details=`dig $host mx +noall +answer | grep MX  | while read i; do echo "$i <br>"; done `;
  echo $mx_details;

  mx_2_details=`dig mx $host +short | awk '{print $2}' | while read i; do dig $i +short; done | while read o; do dig -x $o +short; done | while read p ; do echo "$p <br>"; done | sort -u`

  mx_3_details=`dig $host mx +noall +answer | awk '{print $NF}' | uniq -d`
  if [[ ! -z $mx_3_details ]]; then mx_answer="MX duplicates exist"; else mx_answer="No MX Duplicates"; fi

  domain_name_server=`dig $host ns +short | tail -1`
  mx_40_details=`dig mx $host @@domain_name_server +noall +answer`
  mx_41_details=`dig mx $host @8.8.8.8 +noall +answer`

  if [[ $mx_40_details = $mx_41_details ]] ; then
    mx_4_answer="OK. I did not detect differing IPs for your MX records."
  else
    mx_4_answer="I have differing IP's for your MX records"
  fi

  mx_4_details=`dig $host mx +short | wc -l`

  if [[ $mx_4_details -ge 1 ]] ; then
    mx_5_answer="Good, multiple mail hosts."
  else
    mx_5_answer="Only one MX server detected"
  fi
   
  echo "<tr> <td> MX </td> <td> Number of MX Records </td> <td> $mx_5_answer</td>" 
  echo "<tr> <td> MX </td> <td> Mismatced MX A records </td> <td> $mx_4_answer</td>" 
  echo "<tr> <td valign="top"> MX </td> <td valign="top"> Reverse MX records </td> <td> $mx_2_details </td>" 
  echo "<tr> <td> MX </td> <td> Duplicate MX Records </td> <td> $mx_answer  </td>" 

  # WWW Details
  # WWW Test1 for basic web server records
  www_host=`dig www.$host +noall +answer | grep -v ";" | grep -v "^$" | while read i; do echo "$i <br>"; done `


  # WWW Test2 for private IP in www name
  w1=`dig $host +short | egrep '^192.168|^172.16|^10.'`
  w2=`dig www.$host +short | egrep '^192.168|^172.16|^10.'`
  
  if [ -z $w1 ] || [ -z $w2 ]
    then
      w10="Your IP's appear to be private addresses/"
    else
      w10="All IPs appear publicly accessible.  That is a good thing."
  fi

  # WWW Test3 for CNAME
  w20=`dig $host +noall +answer | tail -2`
  w21=`echo $w20 | grep CNAME`

  if [ -z $w21 ]
    then
      w30="Good, we have a CNAME."
    else
      w30="No CNAME appears for the WWW record."
  fi
  
  # www_host=`dig www.$host +noall +answer | grep -v ";" | grep -v "^$" | while read i; do echo "$i <br>"; done `
  echo "<tr> <td valign="top" rowspan="3"> WWW </td> <td valign="top"> Status </td> <td valign="top"> Web Server Records </td><td>echo $www_host </td></tr>"
  echo "<tr> <td> Status </td> <td> IP's are public </td><td>" $w10 "</td></tr>";
  echo "<tr> <td valign="top"> Status </td> <td valign="top"> WWW CNAME </td><td>" $w30 "<br>" $w20 "</td></tr>";

  echo "</table>"

fi

echo "<br><hr WIDTH=\"90%\" bordercolor="#034f84" SIZE=\"3\">"

echo "</body><br></html>"

exit 0
